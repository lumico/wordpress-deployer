git -v
docker -v
docker-compose -v
git clone git@bitbucket.org:lumico/wordpress-deployer.git .
#git submodule add git@bitbucket.org:GingaLab/digest.git && cd digest && git checkout docker_from_sprint_1.0.2 && cd ..
#git submodule add git@bitbucket.org:GingaLab/ginger_front.git && cd ginger_front && git checkout docker/dev_1.0.2-fix && cd ..
#git submodule init && git submodule update 
docker-compose up --build -d
cp -r wp-content/* /wordpress/wp-content/