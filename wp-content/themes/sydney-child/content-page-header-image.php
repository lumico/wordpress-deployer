<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package Sydney
 */

	$feat_image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()), 'fullwidth' );
?>

<header class="entry-header">
	<div class="page-header-image" style="background-image: url(<?php echo $feat_image; ?>)"></div>
	<div class="page-header-title">
		<?php the_title( '<h1 class="title-post">', '</h1>' ); ?>
	</div>
</header><!-- .entry-header -->

<div class="container">
	<div class="row">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="entry-content">
				<?php the_content(); ?>
			</div><!-- .entry-content -->
		</article><!-- #post-## -->

	</div>
</div>
