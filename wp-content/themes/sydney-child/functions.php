<?php

/*-----------------------------------------------------------------------------------

	Theme CHILD functions.php

	* Your Custom Functions
	* Place your custom functions below

-----------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------
	JS AND CSS LOADING
-----------------------------------------------------------------------------------*/

add_action( 'wp_enqueue_scripts', 'template_load_custom_style_sheet', 999999 );
function template_load_custom_style_sheet() {
	wp_enqueue_style( 'template-stylesheet',  get_template_directory_uri().'-child/css/template.css', array(), 1 );
}

add_action( 'wp_enqueue_scripts', 'load_photoswipe_style_sheet', 10 );
function load_photoswipe_style_sheet() {
	wp_enqueue_style( 'photoswipe-stylesheet',  get_template_directory_uri().'-child/css/photoswipe.css', array(), 1 );
	wp_enqueue_style( 'photoswipe-ui-default-stylesheet',  get_template_directory_uri().'-child/css/default-skin/default-skin.css', array(), 1 );
}

add_action( 'wp_enqueue_scripts', 'load_owcarousel_style_sheet', 10 );
function load_owcarousel_style_sheet() {
	wp_enqueue_style( 'owcarousel-stylesheet',  get_template_directory_uri().'-child/css/owl.carousel.css', array(), 2 );
}

add_action( 'wp_enqueue_scripts', 'load_owcarousel_theme_style_sheet', 10 );
function load_owcarousel_theme_style_sheet() {
	wp_enqueue_style( 'owcarousel-theme-stylesheet',  get_template_directory_uri().'-child/css/owl.theme.default.min.css', array(), 2 );
}

add_action( 'wp_enqueue_scripts', 'custom_load_custom_style_sheet', 999999 );
function custom_load_custom_style_sheet() {
wp_enqueue_style( 'custom-stylesheet',  get_template_directory_uri().'-child/css/custom.css', array(), 1 );
wp_enqueue_style( 'scroll-stylesheet',  get_template_directory_uri().'-child/css/scroll.css', array(), '' );
}

//add_action( 'wp_enqueue_scripts', 'custom_load_custom_responsive_style_sheet', 999999 );
function custom_load_custom_responsive_style_sheet() {
wp_enqueue_style( 'custom-responsive-stylesheet',  get_template_directory_uri().'-child/css/custom-responsive.css', array(), 1 );
}

add_action('wp_enqueue_scripts', 'load_javascript_files', 999999);
function load_javascript_files() {
	wp_register_script('custom_child_js', get_template_directory_uri() . '-child/js/custom.js', array('jquery'), 1, true );
	wp_enqueue_script('custom_child_js');
	wp_register_script('scroll_child_js', get_template_directory_uri() . '-child/js/scroll.js', array('jquery'), '', true );
	wp_enqueue_script('scroll_child_js');
}

add_action('wp_enqueue_scripts', 'replace_main_js_file', 99999);
function replace_main_js_file()
{
	wp_dequeue_script( 'sydney-main' );
	wp_deregister_script( 'sydney-main' );
	wp_register_script( 'sydney-main2', get_template_directory_uri() . '-child/js/main.min.js', array('jquery'),'', true );
	wp_enqueue_script('sydney-main2');
	wp_register_script( 'template-js', get_template_directory_uri() . '-child/js/template.js', array('jquery'),'', true );
	wp_enqueue_script('template-js');

}

/*-----------------------------------------------------------------------------------
	THEME PERSONALIZING
-----------------------------------------------------------------------------------*/

register_nav_menus( array(
	'Footer' => 'Menu Footer',
) );

register_nav_menus( array(
	'Documentation' => 'Menu Documentation',
) );

class Documentation_Walker_Nav_Menu extends Walker_Nav_Menu {
	// Tell Walker where to inherit it's parent and id values
	public function start_lvl( &$output, $depth = 0, $args = array() ) {
		$output .= '<ul class="sub-menu">';
	}

	public function end_lvl( &$output, $depth = 0, $args = array() ) {
		if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
			$t = '';
			$n = '';
		} else {
			$t = "\t";
			$n = "\n";
		}
		$indent = str_repeat( $t, $depth );
		$output .= "$indent</ul>{$n}";
	}

	public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		$classes = array();
		if( !empty( $item->classes ) ) {
			$classes = (array) $item->classes;
		}



		$active_classes = 'menu-item';

		if($depth == 0)
			$active_classes .= ' parent-menu';

		if( in_array('current-menu-item', $classes) ) {
			$active_classes .= ' active';
		} else if( in_array('current-menu-parent', $classes) ) {
			$active_classes .= ' active-parent';
		} else if( in_array('current-menu-ancestor', $classes) ) {
			$active_classes = ' active-ancestor';
		}

		$menu_number = get_field('menu-number', $item);
		$menu_hidden_children = get_field('menu-hidden-children', $item);

		$slug = get_the_slug($item->object_id);

		if($menu_hidden_children)
			$active_classes .= " hidden-children";


		$output .= '<li class="'.$active_classes .'"  content-id="'. $slug . '" >';

		if($depth == 0)
			$output .= '<div class="parent-menu-content">';

		if(!empty($menu_number))
			$output .= '<span class="menu-number">'.$menu_number.'</span>';
		$output .= '<span class="nav-title">' . $item->title . '</span>';
		if($depth == 0) {
			$output .= '<span class="menu-arrow default"><img src="wp-content/uploads/2017/11/right_arrow_white.png" alt=""></span>';
			$output .= '<span class="menu-arrow active"><img src="wp-content/uploads/2017/11/right_arrow_green.png" alt=""></span>';
		}

		if($depth == 0)
			$output .= '</div>';

	}

	public function end_el( &$output, $item, $depth = 0, $args = array() ) {
		$output .= '</li>';
	}
}

// Register the three useful image sizes for use in Add Media modal
add_filter( 'image_size_names_choose', 'wpshout_custom_sizes' );
function wpshout_custom_sizes( $sizes ) {
	return array_merge( $sizes, array(
		'fullwidth' => __( 'Image Fullwidth' ),
	) );
}

add_action( 'after_setup_theme', 'setup' );
function setup() {
	add_image_size( 'fullwidth', 1920, 99999 );
}

function alternative_logo_customize_register( $wp_customize ) {
	$wp_customize->add_setting( 'alternative_logo' ); // Add setting for logo uploader

	// Add control for logo uploader (actual uploader)
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'alternative_logo_logo', array(
		'label'    => __( 'Upload alternative Logo', 'alternative_logo' ),
		'section'  => 'title_tagline',
		'settings' => 'alternative_logo',
	) ) );
}
add_action( 'customize_register', 'alternative_logo_customize_register' );

/*-----------------------------------------------------------------------------------
	UTILS FUNCTIONS
-----------------------------------------------------------------------------------*/

function get_the_slug( $id=null ){
	if( empty($id) ):
		global $post;
		if( empty($post) )
			return ''; // No global $post var available.
		$id = $post->ID;
	endif;

	$slug = basename( get_permalink($id) );
	return $slug;
}

function get_post_featured_image_url($post_id,$size = "full")
{
	$src = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), $size );
	$feat_image = $src[0];
	if(empty($feat_image))
	{
		$feat_image = "";
	}
	return $feat_image;
}

?>