<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

	<!-- End Google Tag Manager -->
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) : ?>
		<?php if ( get_theme_mod('site_favicon') ) : ?>
			<link rel="shortcut icon" href="<?php echo esc_url(get_theme_mod('site_favicon')); ?>" />
		<?php endif; ?>
	<?php endif; ?>

	<?php wp_head(); ?>
	<!--[if lte IE 10]>
	<link rel='stylesheet' href='/wp-content/themes/sydney-child/css/ie.css?ver=4.4.2' type='text/css' media='all' />
	<script>
		document.createElement('header');
		document.createElement('footer');
		document.createElement('hr');
		document.createElement('main');
		document.createElement('article');
	</script>
	<![endif]-->
</head>

<script>
	var alertFallback = false;
	if (typeof console === "undefined" || typeof console.log === "undefined") {
		console = {};
		if (alertFallback) {
			console.log = function(msg) {
				alert(msg);
			};
		} else {
			console.log = function() {};
		}
	}
</script>


<body <?php body_class(); ?>>

<div class="preloader">
	<div class="spinner">
		<div class="pre-bounce1"></div>
		<div class="pre-bounce2"></div>
	</div>
</div>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'sydney' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
		<div class="header-wrap">
			<div class="logo-container">
				<?php if ( get_theme_mod('site_logo') ) : ?>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php bloginfo('name'); ?>"><img class="site-logo" src="<?php echo esc_url(get_theme_mod('site_logo')); ?>" alt="<?php bloginfo('name'); ?>" /></a>
					<div class="logo-vertical-separator"></div>
				<?php endif; ?>
				<div class="side-titles-container">
					<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
					<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
				</div>
			</div>
	</header><!-- #masthead -->
	<?php sydney_slider_template(); ?>

	<div class="header-image">
		<?php sydney_header_overlay(); ?>
		<img class="header-inner" src="<?php header_image(); ?>" width="<?php echo esc_attr( get_custom_header()->width ); ?>" alt="<?php bloginfo('name'); ?>">
	</div>

	<?php
	$menu_id = get_field("nav_menu",get_the_ID());
	wp_nav_menu( array(
		'menu' => $menu_id,
		'container_class' => 'documentation-menu-container',
		'walker' => new Documentation_Walker_Nav_Menu) );
	?>