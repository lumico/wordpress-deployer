/**
 * Created by Thibaut on 21/01/2016.
 */
(function ($){
    var previous_page_id, previous_page_slide = null;
        //get # in url
    var url = window.location.href 
    var pathArray = url.split( '#' );
    console.log("URL is == >" +url);
    console.log(pathArray[1]);
    if(pathArray[1] != undefined){
        load_data_page(pathArray[1],0);
    }

    //$(".screenshot").mCustomScrollbar();
    //$(".documentations-pages-container").css("line-height", ($(window).outerHeight() - 70)+"px" );
    //$(".documentation-text-container").css("line-height", ($(".imac").outerHeight()*0.8)+"px" );
    //$(".documentation-text").css("max-height", ($(".imac").outerHeight()*0.75)+"px" );
    
    function init_page(){
        var grey_content_height = ($(".imac").outerHeight()*0.75);
        $(".documentations-pages-container").css("line-height", ($(window).outerHeight() - 70)+"px" );
        $(".documentation-text-container").css("line-height", ($(".imac").outerHeight()*0.8)+"px" );
        if(grey_content_height > 100) $(".documentation-text").css("max-height", grey_content_height+"px" );
    }

    $(window).on("load", function() {
        init_page();
    });

    $( window ).resize(function() {
        init_page()();
    });

    $('.sub-menu').mouseover(function(e)
    {
        e.stopPropagation();
    });

    $('.documentation-menu-container').click(function(e)
    {
        if(!$(this).hasClass("active"))
            $(this).addClass("active");
        e.stopPropagation();
    });

    $(".entry-content").click(function(e)
    {
        if($('.documentation-menu-container').hasClass("active"))
            $('.documentation-menu-container').removeClass("active");
        e.stopPropagation();
    });

    $(".parent-menu-content").click(function(e)
    {

        if(!$('.documentation-menu-container').hasClass("active"))
            $('.documentation-menu-container').addClass("active");

        var open = false;
        if(!$(this).parent().hasClass("active"))
            open = true;
        $(".parent-menu").removeClass("active");
        if(open) {
            $(this).parent().addClass("active");
            var first_item = $(this).parent().find(".sub-menu .menu-item").first();
            var content_id = first_item.attr("content-id");
            $("ul.sub-menu li.menu-item").removeClass("active");
            first_item.addClass("active");
            load_data_page(content_id,0);
        }
        else {
            $("ul.sub-menu li.menu-item").removeClass("active");
        }
        e.stopPropagation();

    });

    $("ul.sub-menu li.menu-item").click(function(e)
    {

        if($('.documentation-menu-container').hasClass("active"))
             $('.documentation-menu-container').removeClass("active");


        var content_id = $(this).attr("content-id");
        $("ul.sub-menu li.menu-item").removeClass("active");
        $(this).addClass("active")


        e.stopPropagation();
        load_data_page(content_id,0);
    });

    $(".next-button").click(function()
    {
        var content_id = $(".sub-menu .menu-item.active").attr("content-id");

        var documentations_pages_div = $(".documentations-pages-container");

        var slide = parseInt(documentations_pages_div.attr("slide-id"));
        var slide_total = parseInt(documentations_pages_div.attr("slide-total"));

        if(slide < slide_total)
        {
            console.log("Next Slide");
            slide = slide+1;
            load_data_page(content_id,slide);
        }
        else {

            var next_menu_item = $(".sub-menu .menu-item[content-id="+content_id+"]").next();
            console.log(".sub-menu .menu-item[content-id="+content_id+"]");
            if(next_menu_item.length)
            {
                console.log("Next Menu item");
                slide = 0;
                content_id = next_menu_item.attr("content-id");
                load_data_page(content_id,slide);
            }
            else {
                var next_menu_item = $(".sub-menu .menu-item[content-id="+content_id+"]").parent().parent().next();
                if(next_menu_item.length)
                {
                    console.log("Next Menu item");
                    slide = 0;
                    $(".parent-menu").removeClass("active");
                    next_menu_item.addClass("active");
                    content_id = next_menu_item.find(".sub-menu .menu-item").first().attr("content-id");
                    load_data_page(content_id,slide);
                }
            }
        }
    });
    $(".previous-button").click(function()
    {
        var previous_button_div = $(".documentations-pages-container .previous-button");


        var content_id = previous_button_div.attr("previous-page-id");
        var slide = parseInt(previous_button_div.attr("previous-page-slide"));

        if(content_id != null && slide >= 0) {
            load_data_page(content_id, slide);
        }

    });
    // $(".previous-button").click(function()
    // {
    //     var content_id = $(".sub-menu .menu-item.active").attr("content-id");

    //     var documentations_pages_div = $(".documentations-pages-container");

    //     var slide = parseInt(documentations_pages_div.attr("slide-id"));
    //     var slide_total = parseInt(documentations_pages_div.attr("slide-total"));

    //     if(slide > slide_total)
    //     {
    //         console.log("Next Slide");
    //         slide = slide-1;
    //         load_data_page(content_id,slide);
    //     }
    //     else {
    //         var next_menu_item = $(".sub-menu .menu-item[content-id="+content_id+"]").prev();
    //         console.log("Switching to item = "+$(".sub-menu .menu-item[content-id="+content_id+"]").prev().attr("content-id"));
    //         if(next_menu_item.length)
    //         {
    //             console.log("Previous Menu item");
    //             slide = 0;
    //             content_id = next_menu_item.attr("content-id");
    //             load_data_page(content_id,slide);
    //         }
    //         else {
    //             var next_menu_item = $(".sub-menu .menu-item[content-id="+content_id+"]").parent().parent().prev();
    //             if(next_menu_item.length)
    //             {
    //                 console.log("Previous Menu item");
    //                 slide = 0;
    //                 $(".parent-menu").removeClass("active");
    //                 next_menu_item.addClass("active");
    //                 content_id = next_menu_item.find(".sub-menu .menu-item").last().attr("content-id");
    //                 load_data_page(content_id,slide);
    //             }
    //         }
    //     }
    // });

    function load_data_page(id,slide)
    {


        console.log("Loading data [ content-id : "+id+", slide : "+slide+" ]");

        $("ul.sub-menu li.menu-item").removeClass("active");
        $(".sub-menu .menu-item[content-id="+id+"]").addClass("active")

        var screenshot_div = $(".documentations-pages-container .screenshot");
        var screenshot_img_div = $(".documentations-pages-container .screenshot-img");
        var loader_div = $(".documentations-pages-container .loader");
        var logo_div = $(".documentations-pages-container .bnp-logo");
        var documentation_text = $(".documentations-pages-container .documentation-text");
        var previous_button_div = $(".documentations-pages-container .previous-button");
        var next_button_div = $(".documentations-pages-container .next-button");
        var documentations_pages_div = $(".documentations-pages-container");
        var doc_title_div = $(".page-doc-title");
        var doc_subtitle_div = $(".page-doc-subtitle");

        documentations_pages_div.attr("slide-id",slide);
        documentations_pages_div.attr("slide-total",presentation_datas[id].length-1);

        $(".parent-menu").removeClass("active");
        $(".sub-menu .menu-item[content-id="+id+"]").parent().parent().addClass("active");
        
        if(previous_page_id != null)
            previous_button_div.attr("previous-page-id",previous_page_id);
        if(previous_page_slide != null)
            previous_button_div.attr("previous-page-slide",previous_page_slide);

        previous_page_id = id;
        previous_page_slide = slide;


        var delay = 0;
        var delay2 = 500;

        if(documentation_text.parent().hasClass("active"))
            delay = 700;

        if(presentation_datas[id][slide]["first"]){
            previous_button_div.hide();
        }else{
            previous_button_div.show();
        }

        if(!presentation_datas[id][slide]["animation"]){
            delay = 0;
            delay2 = 0;
        }

        screenshot_div.removeClass("active");
        screenshot_img_div.removeClass("active");

        loader_div.addClass("active");
        logo_div.addClass("active");

        documentation_text.parent().removeClass("active");

        // console.log('title = '+$(".sub-menu .menu-item[content-id="+id+"]").parent().parent().find(".nav-title").html());
        screenshot_img_div.attr("src","");


        setTimeout(function()
        {
            screenshot_img_div.attr("src",presentation_datas[id][slide]["doc-page-screenshot"]);
            //screenshot_div.css("background-image","url("+presentation_datas[id][slide]["doc-page-screenshot"]+")");

            documentation_text.html(presentation_datas[id][slide]["doc-page-text"]);
            doc_title_div.html($(".sub-menu .menu-item[content-id="+id+"]").parent().parent().find(".nav-title").html());
            doc_subtitle_div.html($(".sub-menu .menu-item[content-id="+id+"]").find(".nav-title").html());

            if(presentation_datas[id][slide]["previous-button"])
                previous_button_div.html(presentation_datas[id][slide]["previous-button"]);
            if(presentation_datas[id][slide]["next-button"])
                next_button_div.html(presentation_datas[id][slide]["next-button"]);

            loader_div.removeClass("active");
            logo_div.removeClass("active");

            setTimeout(function() {
                screenshot_div.addClass("active");
                screenshot_img_div.addClass("active");
                documentation_text.parent().addClass("active");
            },delay2);

        }, delay);


    }

})(jQuery);


