<?php
/**
 * Loop Name: Archives
 */
?>
<?php if ( have_posts() ) : ?>

    <?php /* Start the Loop */ ?>
    <?php while ( have_posts() ) : the_post(); ?>

         <?php
            $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
        ?>

        <div <?php post_class(); ?> id="post-<?php the_ID(); ?>">

            <?php $post_type = get_post_type_object( get_post_type($post) ); ?>

            <div class="home-article-list">
                <div class="home-article-image" style="background-image: url('<?php echo $feat_image; ?>');"></div>
                <div class="home-article-content">
                    <div class="post-type-label"><?php echo $post_type->labels->singular_name ; ?></div>
                    <h4>
                        <a href="<?php the_permalink(); ?>">
                            <?php the_title(); ?>
                        </a>
                    </h4>

                    <?php if ( has_excerpt( $post->ID ) ) { the_excerpt(); } else {/* nothing */} ?>
                </div>
                <div class="post-date">
                    <div class="black-squared-icon">
                        <img src="/wp-content/uploads/2016/02/ic_clock.png" alt="">
                    </div>
                    <div class="post-date-data">
                        <?php the_date("m/y"); ?>
                    </div>
                </div>
            </div>

        </div>

    <?php endwhile; ?>

<?php else : ?>

    <?php get_template_part( 'no-results', 'index' ); ?>

<?php endif; ?>