<?php
/**
 * Loop Name: Articles home
 */
?>
<?php if ( have_posts() ) : ?>

    <?php /* Start the Loop */ ?>
    <?php while ( have_posts() ) : the_post(); ?>

         <?php
            $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
            if(empty($feat_image))
            {
                $feat_image = "/wp-content/uploads/2016/03/bv_archive.jpg";
            }
        ?>

        <div <?php post_class(); ?> id="post-<?php the_ID(); ?>">

            <?php $post_type = get_post_type_object( get_post_type($post) ); ?>

            <div class="home-article-list">
                <a href="<?php the_permalink(); ?>"> <div class="home-article-image" style="background-image: url('<?php echo $feat_image; ?>');"></div></a>
                <div class="home-article-content">
                    <div class="post-type-label"><?php echo get_cat_name(get_post_meta($post->ID, 'main_category', true));  ?></div>
                    <div class="post-date-responsive">
                            <?php echo get_the_date("m/y"); ?>
                    </div>
                    <h4>
                        <a href="<?php the_permalink(); ?>">
                            <?php the_title(); ?>
                        </a>
                    </h4>

                    <?php if ( has_excerpt( $post->ID ) ) { the_excerpt(); } else {/* nothing */} ?>
                </div>
                <div class="post-date">
                    <div class="black-squared-icon">
                        <img src="/wp-content/uploads/2016/02/ic_clock.png" alt="">
                    </div>
                    <div class="post-date-data">
                        <?php echo get_the_date("m/y",$post->ID ); ?>
                    </div>
                </div>
            </div>

        </div>

    <?php endwhile; ?>

<?php else : ?>

    <?php get_template_part( 'no-results', 'index' ); ?>

<?php endif; ?>