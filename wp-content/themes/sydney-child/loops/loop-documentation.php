<?php
/**
 * Loop Name: Pages de documentation
 */
?>

<div class="documentations-pages-container">

    <div class="imac-container">
        <img class="imac" src="/wp-content/uploads/2017/11/imac.png">
        <!-- <div class="screenshot mCustomScrollbar" data-mcs-theme="screen"> -->
        <div class="screenshot" data-mcs-theme="screen">
            <img class="screenshot-img" src="" />
        </div>
        <img src="/wp-content/uploads/2017/11/LOGO_BNP.png" alt="" class="bnp-logo active">
        <img src="/wp-content/uploads/2017/11/loader.gif" alt="" class="loader">
        <div class="documentation-text-container ">
            <!-- <div class="documentation-text mCustomScrollbar" data-mcs-theme="tab"> -->
            <div class="documentation-text" data-mcs-theme="tab">
            </div>
        </div>
    </div>
    <div class="nav-container">
        <div class="previous-button">
            <
        </div>
        <div class="next-button">
            >
        </div>
    </div>

    <div class="page-doc-title"></div>
    <div class="page-doc-subtitle"></div>

</div>

<?php if ( have_posts() ) : ?>

    <?php

        /* Start the Loop */
        $datas = array();

    ?>

    <?php while ( have_posts() ) : the_post(); ?>

        <?php

            $current_id = get_the_ID();
            $slug = get_the_slug($current_id);

            $datas[$slug] = array();

            $items_data = get_field("doc-page-diapositives",$current_id);

            $i = 0;
            foreach ($items_data as $item_data)
            {
                $datas[$slug][$i]["doc-page-text"] = $item_data["doc-page-text"];
                $datas[$slug][$i]["doc-page-text-position"] = $item_data["doc-page-text-position"];
                $datas[$slug][$i]["doc-page-screenshot"] = $item_data["doc-page-screenshot"]["sizes"]["large"];
                $datas[$slug][$i]["is-multisolutions"] = $item_data["doc-page-is-multisolutions"];
                $datas[$slug][$i]["animation"] = $item_data["doc-page-animation"];
                $datas[$slug][$i]["first"] = $item_data["doc-page-first"];

                $datas[$slug][$i]["previous-button"] = $item_data["doc-page-multisolutions-number"];
                $datas[$slug][$i]["next-button"] = $item_data["doc-page-multisolutions-number"];

                $datas[$slug][$i]["previous-button"] = "<";
                $datas[$slug][$i]["next-button"] = ">";

                if($datas[$slug][$i]["is-multisolutions"])
                {
                    $datas[$slug][$i]["multisolutions-number"] = $item_data["doc-page-multisolutions-number"];
                    $datas[$slug][$i]["previous-button"] = $item_data["doc-page-multisolutions-previous-button-text"];
                    $datas[$slug][$i]["next-button"] = $item_data["doc-page-multisolutions-next-button-text"];
                }
                $i++;
            }

            //echo $current_id;
            //echo $slug;
            //echo $slug = basename(get_permalink());
            //print_r($datas);


        ?>

    <?php endwhile; ?>

    <?php

        if(sizeof($datas))
        {
            echo "<script>";
            echo "var presentation_datas = ".json_encode($datas);
            echo "</script>";
        }

    ?>

<?php else : ?>

    <?php get_template_part( 'no-results', 'index' ); ?>

<?php endif; ?>