<?php

/*

Template Name: Page Documentation

*/
get_header("documentation");
?>



<div id="primary" class="content-area">
    <main id="main" class="site-main other-page" role="main">

        <?php while ( have_posts() ) : the_post(); ?>

            <?php get_template_part( 'content', 'page-documentation' ); ?>

        <?php endwhile; // end of the loop. ?>


    </main><!-- #main -->
</div><!-- #primary -->

<?php get_footer("void"); ?>

