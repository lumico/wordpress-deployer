<?php
/*
Template Name: Search Page
*/

get_header(); ?>

	<div id="primary" class="content-area col-md-12 search-result">
		<main id="main" class="post-wrap" role="main">

            <hr class="mini">
            <h2>
                <?php
                $count = $wp_query->found_posts;
                $several = ($count<=1) ? '' : 's'; //pluriel

                if ($count>0) : $output =  $count.' résultat'.$several.' pour la recherche';
                else : $output = 'Aucun résultat pour la recherche';
                endif;

                $output .= ' "<span class="terms_search">'. get_search_query() .'</span>"';

                echo $output;
                ?>
            </h2>


            <?php if (have_posts()) : ?>

                <?php /* Start the Loop */ ?>
                <?php while (have_posts()) : the_post(); ?>

                    <?php
                    $feat_image = wp_get_attachment_url(get_post_thumbnail_id($cpost->ID));
                    if(empty($feat_image))
                        $feat_image = "/wp-content/uploads/2016/03/bv_archive.jpg";
                    ?>

                    <div <?php post_class(); ?> id="post-<?php the_ID(); ?>">

                        <?php $post_type = get_post_type_object( get_post_type($post) ); ?>

                        <div class="home-article-list">
                            <a href="<?php the_permalink(); ?>"><div class="home-article-image" style="background-image: url('<?php echo $feat_image; ?>');"></div></a>
                            <div class="home-article-content">
                                <div class="post-type-label">
                                    <?php
                                        $main_cat = get_cat_name(get_post_meta($cpost->ID, 'main_category', true));
                                        if(empty($main_cat))
                                            $main_cat = get_the_category()[0]->name;
                                        echo $main_cat;
                                    ?></div>
                                <h4>
                                    <a href="<?php the_permalink(); ?>">
                                        <?php the_title(); ?>
                                    </a>
                                </h4>

                                <?php if ( has_excerpt( $post->ID ) ) { the_excerpt(); } else {/* nothing */} ?>
                            </div>
                            <div class="post-date">
                                <div class="black-squared-icon">
                                    <img src="/wp-content/uploads/2016/02/ic_clock.png" alt="">
                                </div>
                                <div class="post-date-data">
                                    <?php echo get_the_date("m/y",$post->ID ); ?>
                                </div>
                            </div>
                        </div>

                    </div>

                <?php endwhile; ?>
                <div class="search-pagination">
                    <div class="alignleft"><?php previous_posts_link( '&laquo; Résultats précédents' ); ?></div>
                    <div class="alignright"><?php next_posts_link( 'Résultat suivants &raquo;', '' ); ?></div>
                </div>
            <?php else : ?>

                <h2 class="center">Aucun article trouvé. Essayez une nouvelle recherche.</h2>

	        <?php endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php //get_sidebar(); ?>
<?php get_footer(); ?>
