<?php
/**
 * The template for displaying search forms
 *

 */
?>

<?php



?>


<div id="search">
    <form method="get" id="searchform" action="<?php bloginfo('url'); ?>/">
        <div>
            <div class="search-input-container">
                <input type="text" value="<?php the_search_query(); ?>" name="s" id="s" placeholder="RECHERCHE" />
            </div>
            <div class="white-search-container">
                <div class="search-form-title">Chercher par date</div>
                <div class="search-form-date"><span class="input-label-beside">De</span><input type="text" name="after" placeholder="MM/AAAA" value="<?php if(!empty($_GET["after"])) echo
                    $_GET["after"]; ?>"></div>
                <div class="search-form-date"><span class="input-label-beside">à</span><input type="text" name="before" placeholder="MM/AAAA" value="<?php if(!empty($_GET["before"])) echo
                    $_GET["before"]; ?>"></div>
                <div class="search-form-spacer"></div>
                <div class="search-form-title">Chercher par rubrique</div>
                <div class="custom-dropdown">
                    <select name="" id="custom_rub_selector">
                        <option value=""></option>
                        <?php

                            $args = array(
                                'orderby' => 'id',
                                'order' => 'ASC',
                                'hide_empty'=> 0,
                                'taxonomy' => 'category'
                            );
                            $categories = get_categories($args);
                            if(!empty($categories)) {
                                foreach ($categories as $cat) {
                                    echo '<option class="level-0" value="' . $cat->term_id . '">';
                                    echo $cat->name;
                                    echo '</option>';
                                }
                            }

                        ?>
                    </select>
                </div>
                <div class="search-form-title">Chercher par activité</div>
                <div class="custom-dropdown">
                    <select name="" id="custom_cat_selector">
                        <option value=""></option>
                        <?php

                        $args = array(
                            'orderby' => 'id',
                            'order' => 'ASC',
                            'hide_empty'=> 0
                        );
                        $taxonomies = get_terms('activite',$args);
                        if(!empty($taxonomies)) {
                            foreach ($taxonomies as $tax) {
                                echo '<option class="level-0" value="' . $tax->term_id . '">';
                                echo $tax->name;
                                echo '</option>';
                            }
                        }

                        ?>
                    </select>
                </div>
                <div class="search-form-title">Vous recherchez : </div>
                <div id="search-form-selected-filter">
                    <?php if(!empty($_GET['activite'])) : ?>

                        <?php foreach($_GET['activite'] as $filter) :  ?>

                            <div class="search-form-filter"><div class="value"><?php echo get_cat_name($filter) ?></div><div class="remove"></div><input type="hidden" name="activite[]" value="<?php
                                echo $filter ?>"></div>

                        <?php endforeach; ?>
                    <?php endif; ?>

                    <?php if(!empty($_GET['rubrique'])) : ?>

                        <?php foreach($_GET['rubrique'] as $filter) :  ?>

                            <div class="search-form-filter"><div class="value"><?php echo get_cat_name($filter) ?></div><div class="remove"></div><input type="hidden" name="rubrique[]" value="<?php
                                echo $filter ?>"></div>

                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
                <input type="submit" id="searchsubmit" value="Rechercher" />
            </div>
        </div>
    </form>
</div>