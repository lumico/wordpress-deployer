<?php
/**
 * Vertical separator
 *
 * @package Bureau Veritas
 */


class vertical_separator extends WP_Widget {

    public function __construct() {
        $widget_ops = array('classname' => 'vertical_separator', 'description' => __('Separate vertically content with custom height', 'Holaf'));
        $control_ops = array('width' => 400, 'height' => 350);
        parent::__construct('widget', "Vertical Separator", $widget_ops, $control_ops);
    }

    public function widget( $args, $instance ) {

        $html = "<div class='vertical-separator";
        $class = "";
        if ( isset( $instance[ 'responsive' ] ) ) {
            if(!$instance[ 'responsive' ])
                $class = " responsive-hidden";
        }
        $html .= $class."'";
        if ( isset( $instance[ 'height' ] ) ) {
            $height = $instance[ 'height' ];
            $html .= " style='height:$height'";
        }
        $html .= "></div>";
        echo $html;
    }

    function form( $instance ) {
        if ( isset( $instance[ 'height' ] ) ) {
            $height = $instance[ 'height' ];
        }
        else {
            $height = "0px";
        }
        if ( isset( $instance[ 'responsive' ] ) ) {
            $responsive = $instance[ 'responsive' ];
        }
        else {
            $responsive = false;
        }
        ?>
        <h4>
            Choose the height of the separator and his responsive behavior.
        </h4>
        <p>
            <label for="<?php echo $this->get_field_id( 'height' ); ?>"><?php _e( 'Height (px, %, pc ...) :' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'height' ); ?>" name="<?php echo $this->get_field_name( 'height' ); ?>" type="text" value="<?php echo esc_attr( $height ); ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'responsive' ); ?>"><?php _e( 'Responsive visible :' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'responsive' ); ?>" name="<?php echo $this->get_field_name( 'responsive' ); ?>" type="checkbox" value="<?php echo esc_attr(
                $responsive ); ?>">
        </p>
        <?php
    }

    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['height'] = ( ! empty( $new_instance['height'] ) ) ? strip_tags( $new_instance['height'] ) : '';
        $instance['responsive'] = ( ! empty( $new_instance['responsive'] ) ) ? strip_tags( $new_instance['responsive'] ) : '';
        return $instance;
    }
}

